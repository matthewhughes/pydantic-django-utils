Pydanic Django Utils
====================

Some utils for working with `Django <https://www.djangoproject.com/>`_ and
`Pydantic <https://pydantic-docs.helpmanual.io/>`_ (and `Django REST Framework
<https://www.django-rest-framework.org/>`_).

More info in `the docs
<https://matthewhughes.gitlab.io/pydantic-django-utils/index.html>`_
