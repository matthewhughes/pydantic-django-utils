.. testsetup:: *

   import os

   import pprint

   # Just to make printing a bit prettier
   # without making the test code too noisy
   def print(str):
      pprint.pprint(str)

   # same hack as in tests to get Djagno playing nicely
   os.environ["DJANGO_SETTINGS_MODULE"] = "tests.settings"

Examples
========

OpenAPI Utils
~~~~~~~~~~~~~

:func:`pydantic_django_utils.pydantic_openapi_params` can build params from a basic model:


.. testcode::

   from pydantic import BaseModel
   from pydantic_django_utils import pydantic_openapi_params

   class Model(BaseModel):
      foo: int

   print(pydantic_openapi_params(Model))

.. testoutput::

   [{'allowEmptyValue': False,
     'deprecated': False,
     'description': '',
     'in': 'query',
     'name': 'foo',
     'required': True}]


:attr:`pydantic_django_utils.ParameterDict.required` is set according to the
type of the variable:

.. testcode::

   from typing import Optional
   from pydantic import BaseModel
   from pydantic_django_utils import pydantic_openapi_params

   class Model(BaseModel):
      required_param: int
      optional_param: Optional[int]

   print(pydantic_openapi_params(Model))

.. testoutput::

   [{'allowEmptyValue': False,
     'deprecated': False,
     'description': '',
     'in': 'query',
     'name': 'required_param',
     'required': True},
    {'allowEmptyValue': False,
     'deprecated': False,
     'description': '',
     'in': 'query',
     'name': 'optional_param',
     'required': False}]

Other fields can be set through the field's info:

.. testcode::

   from pydantic import BaseModel, Field
   from pydantic_django_utils import pydantic_openapi_params

   class WithDescription(BaseModel):
      described_param: str = Field(
         description="This is a very interesting parameter!"
      )

   class InPath(BaseModel):
      path_param: str = Field(location="path")

   class WithDeprecated(BaseModel):
      deprecated_field: bool = Field(deprecated=True)

   class WithNoAllowEmpty(BaseModel):
      can_be_empty: bool = Field(allowEmptyValue=False)

   print(pydantic_openapi_params(WithDescription)[0]["description"])
   print(pydantic_openapi_params(InPath)[0]["in"])
   print(pydantic_openapi_params(WithDeprecated)[0]["deprecated"])
   print(pydantic_openapi_params(WithNoAllowEmpty)[0]["allowEmptyValue"])

.. testoutput::

   'This is a very interesting parameter!'
   'path'
   True
   False

Django Utils
~~~~~~~~~~~~

:func:`pydantic_django_utils.querydict_to_dict` and
:class:`pydantic_django_utils.QueryDictModel` are conveniences for building
a :class:`pydantic.BaseModel` from a :class:`django.QueryDict`.

.. testcode::

   from typing import List
   from django.http import QueryDict
   from pydantic import BaseModel
   from pydantic_django_utils import QueryDictModel, querydict_to_dict

   class Model(BaseModel):
      single_param: int
      list_param: List[str]

   class QueryModel(QueryDictModel):
      single_param: int
      list_param: List[str]

   query_dict = QueryDict("single_param=12&list_param=hello")

   print(Model.parse_obj(querydict_to_dict(query_dict, Model)))
   print(QueryModel.parse_obj(query_dict))

.. testoutput::

   Model(single_param=12, list_param=['hello'])
   QueryModel(single_param=12, list_param=['hello'])

Django Rest Framework Utils
~~~~~~~~~~~~~~~~~~~~~~~~~~~

:func:`pydantic_django_utils.to_drf_error_details` will propagate any errors from Pydantic:

.. testcode::

   from pydantic import BaseModel, ValidationError
   from pydantic_django_utils import to_drf_error_details

   class Model(BaseModel):
      foo: int
      bar: str

   data = {"foo": "ok"}

   try:
      Model.parse_obj(data)
   except ValidationError as e:
      print(to_drf_error_details(e))

.. testoutput::

   {'bar': ['field required'], 'foo': ['value is not a valid integer']}


Errors descend into nested fields:

.. testcode::

   from typing import List
   from pydantic import BaseModel, ValidationError
   from pydantic_django_utils import to_drf_error_details

   class Child(BaseModel):
      user_id: int

   class Parent(BaseModel):
      children: List[Child]

   data = {"children": [{"user_id": "not_a_number"}, {}]}
   expected_details = {
      "children": {
         "0": {"user_id": ["value is not a valid integer"]},
         "1": {"user_id": ["field required"]},
      }
   }

   try:
      Parent.parse_obj(data)
   except ValidationError as e:
      print(to_drf_error_details(e))

.. testoutput::

    {'children': {'0': {'user_id': ['value is not a valid integer']},
                  '1': {'user_id': ['field required']}}}
