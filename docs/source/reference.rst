Reference
=========

OpenAPI Utils
~~~~~~~~~~~~~

.. autoclass:: pydantic_django_utils.ParameterDict
   :members:

.. autofunction:: pydantic_django_utils.pydantic_openapi_params

Django Utils
~~~~~~~~~~~~

.. autoclass:: pydantic_django_utils.QueryDictModel
   :members:
   :undoc-members:

.. autofunction:: pydantic_django_utils.querydict_to_dict

Django Rest Framework Utils
~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. autofunction:: pydantic_django_utils.to_drf_error_details
