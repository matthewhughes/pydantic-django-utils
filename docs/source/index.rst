.. pydantic-django-utils documentation master file, created by
   sphinx-quickstart on Sun Aug 15 20:10:28 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

pydantic-django-utils
=================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   reference
   examples


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
