import re

import pydantic_django_utils


def test_version_is_valid() -> None:
    assert re.match(r"\d+.\d+.\d+", pydantic_django_utils.__version__)
