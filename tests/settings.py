# (mostly) empty settings file just so we can pass Django _something_
# required for QueryDict (see https://code.djangoproject.com/ticket/27389),
# and django-stubs

# Avoid default value warning for Django 5
USE_TZ = False
