import sys

if sys.version_info >= (3, 8):
    import importlib.metadata

    version = importlib.metadata.version("pydantic-django-utils")
else:
    from typing import cast

    import importlib_metadata

    version = cast(str, importlib_metadata.version("pydantic-django-utils"))
