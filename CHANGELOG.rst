CHANGELOG
=========

0.0.2
-----

Features
~~~~~~~~

* Add Python 3.10 support
* Drop python 3.6 support
