stages:
    - test
    - test:coverage
    - release:docs
    - release:gitlab

# setup basic env for running tox
.setup-base:
    only:
        - merge_request
        - tags
        - master

.tox-base:
    before_script:
        - pip install tox
    variables:
        TOX_DIR: ${CI_PROJECT_DIR}/.tox
    cache:
        key: tox-${CI_JOB_NAME}
        paths:
            - ${TOX_DIR}/${TOXENV}

.pytest-base:
    after_script:
        - cp .coverage "${CI_JOB_NAME}.coverage"
    artifacts:
        when: on_success
        paths:
            - "${CI_JOB_NAME}.coverage"

.test-37-base:
    extends: .setup-base
    stage: test
    image: python:3.7

.test-38-base:
    extends: .setup-base
    stage: test
    image: python:3.8

.test-39-base:
    extends: .setup-base
    stage: test
    image: python:3.9

.test-310-base:
    extends: .setup-base
    stage: test
    image: python:3.10


test-3.7:
    variables:
        TOXENV: py37
    extends:
        - .tox-base
        - .test-37-base
        - .pytest-base
    script:
        - tox

test-3.8:
    variables:
        TOXENV: py38
    extends:
        - .tox-base
        - .test-38-base
        - .pytest-base
    script:
        - tox

test-3.9:
    variables:
        TOXENV: py39
    extends:
        - .tox-base
        - .test-39-base
        - .pytest-base
    script:
        - tox

test-3.10:
    variables:
        TOXENV: py310
    extends:
        - .tox-base
        - .test-310-base
        - .pytest-base
    script:
        - tox

test-docs:
    extends:
        - .test-38-base
        - .tox-base
    script:
        - tox -e docs
        - tox -e docs-testing
    artifacts:
        when: on_success
        paths:
            - docs/build/html

.pre-commit-base:
    before_script:
        - pip install pre-commit
    variables:
        PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
    script:
        - pre-commit run --all-files
    cache:
        key: pre-commit-${CI_JOB_NAME}
        paths:
            - ${PRE_COMMIT_HOME}

lint-3.7:
    extends:
        - .test-37-base
        - .pre-commit-base

lint-3.8:
    extends:
        - .test-38-base
        - .pre-commit-base

lint-3.9:
    extends:
        - .test-39-base
        - .pre-commit-base

lint-3.10:
    extends:
        - .test-310-base
        - .pre-commit-base

report-coverage:
    stage: test:coverage
    extends: .setup-base
    image: python:3.8
    before_script:
        - pip install coverage
    script:
        - coverage combine *.coverage
        - coverage report --fail-under 100
    needs:
        - job: test-3.7
        - job: test-3.8
        - job: test-3.9
        - job: test-3.10

# Release conditions: a tag like 'v<major>.<minor>.<patch>' where each <> is
# numeric and the trailing '.<patch>' is optional. These tags are protected
# within gitlab
.release-candidate:
    rules:
        - if: '$CI_COMMIT_TAG =~ /^v\d+\.\d+(?:\.\d+)?$/'

# release hosted docs
# https://docs.gitlab.com/ee/user/project/pages/#how-it-works
pages:
    extends: .release-candidate
    stage: release:docs
    needs:
        - job: test-docs
    script:
        # See docs, but GitLab Pages deploys from the 'public/' dir
        - cp --recursive docs/build/html public/
    artifacts:
        when: on_success
        paths:
            - public/

.release-code:
    extends: .release-candidate
    # Always pass artifacts down to ensure they're present in the final release
    artifacts:
        paths:
            - dist/*.whl
            - dist/*.tar.gz
        when: on_success

release-gitlab:
    stage: release:gitlab
    extends: .release-code
    image: python
    before_script:
        - pip3 install setuptools wheel
    script:
        - python setup.py sdist bdist_wheel
